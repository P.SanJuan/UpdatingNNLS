%% COMPUTES THE UPDATING OF THE NNLS PROBLEM min(Norm(Ax-b)) 
% SUBJECT TO A x>=0
% Implementation of Portugal & Judice BLOCK PIVOTING method
% Extracted form Kim & Park paper.
% Returns the problem solution, x, 
% and the exchange number between sets cF and cG, e.
%
% cF and Cg initialized by user
%

function [x,e]=IterateNNLS(A,b,x,y,cF,cG)

[m,n]=size(A);

if m ~= size(b,1)
    disp('ERROR: dimension inconsistence in b')
end


% Initialization of algorithm parameters
Alfa=3;
Beta=n+1;

SALIR=1;
for i=1:n
    if (y(i)<0 || x(i)*y(i)~=0)
        SALIR=0;
    end
end    

contador=0;
e=0;

%%%%%%%%  MAIN LOOP %%%%%%%%%

while SALIR==0

    contador=contador+1;
    
    % Update V
    V=[];
    for i=1:numel(cF)
        if x(cF(i))<0
            V=union(V,[cF(i)]);
        end
    end
    
    for i=1:numel(cG)
        if y(cG(i))<0
            V=union(V,[cG(i)]);
        end
    end
    
    
    if numel(V)<Beta
        Beta=numel(V);Alfa=3;Vg=V; 
    elseif Alfa>=1 
           Alfa=Alfa-1;Vg=V;
    elseif Alfa==0
           maxi=max(V);
           Vg=[];
           for i=1:numel(V);
               if V(i)==maxi
                   Vg=union(Vg,[V(i)]);
               end
           end
    end     


    % Update sets F and G
   
    F=setdiff(cF,Vg);F1=intersect(Vg,cG);
    G=setdiff(cG,Vg);G1=intersect(Vg,cF);
    
    % count exchanges
    Fb=cF;
    Gb=cG;
    

    cF=union(F,F1);
    cG=union(G,G1);

    % count exchanges
    
    e=e+(numel(cF)-numel(intersect(cF,Fb)))+(numel(cG)-numel(intersect(cG,Gb)));
    
    % Update xF and yG     
    clear xF yG AF AG
    
    for i=1:numel(cF)
        AF(:,i)=A(:,cF(i));
    end
    
    xF=AF\b;
     
    x=zeros(n,1);bg=-b;
    for i=1:numel(cF)
        x(cF(i))=xF(i);
        bg=bg+A(:,cF(i))*xF(i);
    end
    
    

    
    %bg=(A*x-b)
    y=zeros(n,1);
    for i=1:numel(cG)
        y(cG(i))=A(:,cG(i))'*bg;
    end
    
    
    SALIR=1;
    for i=1:numel(cF)
        if xF(i)<0
            SALIR=0;
        end
    end
    
    for i=1:numel(cG)
        if y(cG(i))<0
            SALIR=0;
        end
    end
    
    

end

end


