%% COMPUTES THE NNLS PROBLEM min(Norm(Ax-b)) 
% SUBJECT TO A x>=0 by initializing the IterateNNLS
% algorithm to the default parameters of the BPP algorith 
%
% Implementation of Portugal & Judice BLOCK PIVOTING method
% Extracted form Kim & Park paper.
% Returns the problem solution, x, 
% and the exchange number between sets cF and cG, e.
function [x,e]= NNLS_BPP(A,b)

    [m,n] =size(A);
    %default BPP parameters
    x=zeros(n,1);
    y=-A'*b;
    cF=[];cG=[1:n];

    [x,e] = IterateNNLS(A,b,x,y,cF,cG);

end