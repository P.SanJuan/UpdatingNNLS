% COMPUTES THE DOWNDATING OF THE NNLS PROBLEM min(Norm(Ax-b)) 
% SUBJECT TO A x>=0
% when a column is removed
%   
% GIVEN A(mxn), b(mx1), w=NNLS(A,b)(nx1)
% COMPUTES the solution of x=NNLS(A1,b), x((n-r)x1), A1(mx(n-r))
% taking advantage of the structure of w


function [x,e]=NNLS_DOWN_Cr(A,b,r,w)

   [m,n]=size(A);

    if m ~= size(b,1) 
        disp('ERROR: dimension inconsistence in b')
    end
    if n ~= size(w,1)
        disp('ERROR: dimension inconsistence in w')
    end

    %Checking if x is a valid solution
    x=w(1:n-r);
    e=0;
    
    if sum(w(n-r+1:n)) ~= 0
        
        %Checking KKT conditions
        A1=A(:,1:n-r);
        y1=A1'*(A1*x-b);
        
        SOLUTION=1;
        for i=1:n-r
            if (y1(i)<0) || (x(i)*y1(i) ~= 0)
               SOLUTION=0;
            end    
        end  


        if SOLUTION==0
            % set selection for block pivoting initialization
            cF=[];
            for i=1:n-r
                if x(i) > 0
                    cF=[cF i];
                end
            end
            cG=setdiff([1:n-r],cF); 
            
             %Launch block pivoting iteration
            [x,e]=IterateNNLS(A1,b,x,y1,cF,cG);
        end
    end
end



