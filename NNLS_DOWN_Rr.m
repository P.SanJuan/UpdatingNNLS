% CALCULA la solucion x=NNLS([A(m-k,:)],[b(1:m-k)])
% COMPUTES THE DOWNDATING OF THE NNLS PROBLEM min(Norm(Ax-b)) 
% SUBJECT TO A x>=0
% when a row block is removed from A and a vector is removed from b
%   
% GIVEN A(mxn), b(mx1), w=NNLS(A,b)(nx1)
% COMPUTES the solution of x=NNLS(A1,b), x(nx1), A1((m-r)xn), b1((m-r)x1 
% taking advantage of the structure of w


function [x,e]=NNLS_DOWN_Rr(A,b,r,w)

    [m,n]=size(A);

    if m ~= size(b,1) 
        disp('ERROR: dimension inconsistence in b')
    end
    if n ~= size(w,1)
        disp('ERROR: dimension inconsistence in w')
    end

    %Exact solution validation
    accuracy=0.000000001;
    if norm(A(1:m-r,:)*w-b(1:m-r))<accuracy
        x=w;e=0;
    else
    
    
        %Checking if x is a valid solution
        x=w;
        y=A'*(A*x-b);
        Aux = A(m-r+1:m,:)' * (A(m-r+1:m,:)*x - b(m-r+1:m));
        y1 = y - Aux;
        e=0;

        SALIR=1; i=1;
        while SALIR==1 && i<n+1     
            if (y1(i)<0 || y1(i)*x(i)~=0)
                SALIR=0;
            end
            i=i+1;
        end

        if SALIR==0
            % set selection for block pivoting initialization
            cF=[];
            for i=1:n
                if x(i) > 0
                    cF=[cF i];
                end
            end
            cG=setdiff([1:n],cF); 
            %initialization of A and b 
            A=A(1:m-r,:);
            b=b(1:m-r);
             %Launch block pivoting iteration
            [x,e]=IterateNNLS(A,b,x,y1,cF,cG);        
        end    
    end
end    


