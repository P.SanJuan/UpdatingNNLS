% COMPUTES THE UPDATING OF THE NNLS PROBLEM min(Norm(Ax-b)) 
% SUBJECT TO A x>=0
% when a new column is added
%   
% GIVEN A(mxn), b(mx1), v(mx1), w=NNLS(A,b)(nx1)
% COMPUTES the solution of x=NNLS([A v],b)
% taking advantage of the structure of w




function [x,e]=NNLS_UP_C1(A,b,v,w)

    [m,n]=size(A);

    if m ~= size(b,1) 
        disp('ERROR: dimension inconsistence in b')
    end
    if m ~= size(v,1) 
        disp('ERROR: dimension inconsistence in v')
    end
    if n ~= size(w,1)
        disp('ERROR: dimension inconsistence in w')
    end
    
    %Checking if x is a valid solution
    bg=(A*w-b);
    y1n=v'*bg;
    x=[w;0];
    e=0;
    
    if y1n < 0
        % set selection for block pivoting initialization
        cF=[];
        for i=1:n
            if w(i) > 0
                cF=[cF i];
            end
        end
        cG=setdiff([1:n+1],cF); 
        %initialization of A and y
        y1=[A'*bg;y1n];
        A=[A v];
        %Launch block pivoting iteration
        [x,e]=IterateNNLS(A,b,x,y1,cF,cG);
    end  
 
end



  
