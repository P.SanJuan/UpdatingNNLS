% COMPUTES THE UPDATING OF THE NNLS PROBLEM min(Norm(Ax-b)) 
% SUBJECT TO A x>=0
% when a new column block is added
% It updates all columns at the same time
%   
% GIVEN A(mxn), b(mx1), V(mxr), w=NNLS(A,b)(nx1)
% COMPUTES the solution of x=NNLS([A v],b)
% taking advantage of the structure of w




function [x,e]=NNLS_UP_Cr(A,b,V,w)

    [m,n]=size(A);
    r=size(V,2);

    if m ~= size(b,1) 
        disp('ERROR: dimension inconsistence in b')
    end
    if m ~= size(V,1) 
        disp('ERROR: dimension inconsistence in V')
    end
    if n ~= size(w,1)
        disp('ERROR: dimension inconsistence in w')
    end
    
    %Checking if x is a valid solution
    bg=(A*w-b);
    y2=V'*bg;
    x=[w;zeros(r,1)];
    e=0;

    SOLUTION=true; 
    for i=1:r
        if y2(i)<0
             SOLUTION=false;
        end
    end
    
    
    if SOLUTION==false
        % set selection for block pivoting initialization
        cF=[];
        for i=1:n
            if w(i) > 0
                cF=[cF i];
            end
        end
        cG=setdiff([1:n+r],cF); 
        %initialization of A and y
        y1=[A'*bg;y2];
        A=[A V];
        %Launch block pivoting iteration
        [x,e]=IterateNNLS(A,b,x,y1,cF,cG);
    end  
 
end



  
