% COMPUTES THE UPDATING OF THE NNLS PROBLEM min(Norm(Ax-b)) 
% SUBJECT TO A x>=0
% when a new row is added to A and ONE ELEMENT is added to b
%   
% GIVEN A(mxn), b(mx1), v(nx1), t(1x1), w=NNLS(A,b)(nx1)
% COMPUTES the solution ofx=NNLS([A;v'],[b;t])
% taking advantage of the structure of w

function [x,e]=NNLS_UP_R1(A,b,v,t,w)

    [m,n]=size(A);

    if m ~= size(b,1) 
        disp('ERROR: dimension inconsistence in b')
    end
    if n ~= size(v,1) 
        disp('ERROR: dimension inconsistence in v')
    end
    if n ~= size(w,1)
        disp('ERROR: dimension inconsistence in w')
    end

    
    %Exact solution validation
    accuracy=0.000000001;
    if norm([A;v']*w-[b;t])<accuracy
        x=w;e=0;
    else
        
        %Checking if x is a valid solution
        x=w;
        Aux=(v'*x-t)*v;
        y=A'*(A*x-b);
        e = 0;

        SOLUTION=1; i=1;
        while SOLUTION==1 && i<n+1
            if (Aux(i)+y(i)<0 || Aux(i)*x(i)~=0 )
                SOLUTION=0;
            end
            i=i+1;   
        end

        if SOLUTION==0
            % set selection for block pivoting initialization
            cF=[];
            for i=1:n
                if x(i) > 0
                    cF=[cF i];
                end
            end
            cG=setdiff([1:n],cF);
            %initialization of A, b and y
            A=[A;v'];
            b=[b;t];
            y=y+Aux;
             %Launch block pivoting iteration
            [x,e]=IterateNNLS(A,b,x,y,cF,cG);    
        end
    end
end


