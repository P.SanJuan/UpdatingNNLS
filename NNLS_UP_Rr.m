% COMPUTES THE UPDATING OF THE NNLS PROBLEM min(Norm(Ax-b)) 
% SUBJECT TO A x>=0
% when a new row block is added to A and a VECTORT is added to b
%   
% GIVEN A(mxn), b(mx1), v(nxr), t(rx1), w=NNLS(A,b)(nx1)
% COMPUTES the solution ofx=NNLS([A;V'],[b;t])
% taking advantage of the structure of w

function [x,e]=NNLS_UP_Rr(A,b,V,t,w)

    [m,n]=size(A);

    if m ~= size(b,1) 
        disp('ERROR: dimension inconsistence in b')
    end
    if n ~= size(V,1) 
        disp('ERROR: dimension inconsistence in v')
    end
    if n ~= size(w,1)
        disp('ERROR: dimension inconsistence in w')
    end

    %Exact solution validation
    accuracy=0.000000001;
    if norm([A;V']*w-[b;t])<accuracy
        x=w;e=0;
    else
    
        %Checking if x is a valid solution
        x=w;
        Aux=V*(V'*x-t);
        y=A'*(A*x-b);
        e = 0;

        SOLUTION=1; i=1;
        while SOLUTION==1 && i<n+1
            if (Aux(i)+y(i)<0 || Aux(i)*x(i)~=0)
                SOLUTION=0;
            end
            i=i+1;   
        end

        if SOLUTION==0
            % set selection for block pivoting initialization
            cF=[];
            for i=1:n
                if x(i) > 0
                    cF=[cF i];
                end
            end
            cG=setdiff([1:n],cF);
            %initialization of A, b and y
            A=[A;V'];
            b=[b;t];
            y=y+Aux;
             %Launch block pivoting iteration
            [x,e]=IterateNNLS(A,b,x,y,cF,cG);    
        end
    end
end
 
 

 

