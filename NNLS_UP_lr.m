% COMPUTES THE UPDATING OF THE NNLS PROBLEM min(Norm(Ax-b)) 
% SUBJECT TO A x>=0
% when a low rank matrix is added
%   
% GIVEN A(mxn), b(mx1), V(mxr), Z(nxr), w=NNLS(A,b)(nx1)
% COMPUTES the solution of x=NNLS([A+V*Z'],b)
% taking advantage of the structure of w


function [x,e]=NNLS_UP_lr(A,b,V,Z,w)

    [m,n]=size(A);

    if m ~= size(b,1) 
        disp('ERROR: dimension inconsistence in b')
    end
    if m ~= size(V,1) 
        disp('ERROR: dimension inconsistence in v')
    end
    if n ~= size(w,1)
        disp('ERROR: dimension inconsistence in w')
    end
    if n ~= size(Z,1)
        disp('ERROR: dimension inconsistence in z')
    end
    
    if size(V,2) ~= size(Z,2)
        disp('ERROR: dimension inconsistence in product inner dimension')
    end
    

    %Checking if x is a valid solution
    %Simplified version of lines 1-5 of algorithm 12
    x=w;
    c = A*x-b;
    y = A' * c;
    p = V*(Z'*x);
    Aux= A'*p + Z*(V'*(c+p));
    y1 = y + Aux;
    e=0;

    SALIR=1; i=1;
    while SALIR==1 && i<n+1     
        if (y1(i)<0 || y1(i)*x(i)~=0)
            SALIR=0;
        end
        i=i+1;
    end

    if SALIR==0
        % set selection for block pivoting initialization
        cF=[];
        for i=1:n
            if x(i) > 0
                cF=[cF i];
            end
        end
        cG=setdiff([1:n],cF); 
        %initialization of A 
        A=A+V*Z';
        %Launch block pivoting iteration
        [x,e]=IterateNNLS(A,b,x,y1,cF,cG);
    end
end

